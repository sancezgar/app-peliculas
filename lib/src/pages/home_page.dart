import 'package:flutter/material.dart';
import 'package:peliculas/src/providers/pelicula_provider.dart';
import 'package:peliculas/src/search/search_delegate.dart';
import 'package:peliculas/src/widgets/card_swiper_widget.dart';
import 'package:peliculas/src/widgets/movie_horizontal.dart';


class HomePage extends StatelessWidget {

  final peliculaProvider = new PeliculaProvider();

  @override
  Widget build(BuildContext context) {

    peliculaProvider.getPopulares();

    return Scaffold(
      appBar: AppBar(
        title: Text('Peliculas app'),
        actions: <Widget>[
         IconButton(icon: Icon(Icons.search), onPressed: (){

          showSearch(context: context,delegate: PeliSearch());

         })
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          crearSwiper(),
          crearSwiperHorizontal(context)
        ],
      ),
    );
  }

  Widget crearSwiper() {

    final peliculaProvider = new PeliculaProvider();
    

    return FutureBuilder(
      future: peliculaProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {

        if(snapshot.hasData){ 
          //print(snapshot.data);
          return CardSwiper(peliculas: snapshot.data,); 
        }
        else return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
                child: CircularProgressIndicator()
            ),
          ],
        );


      },
    );
  }

  Widget crearSwiperHorizontal(BuildContext context){

    

    return Container(
      //width: double.infinity,
      child: Column(

        crossAxisAlignment: CrossAxisAlignment.start,

        children: <Widget>[

          Container(
            margin: EdgeInsets.only(left:20.0),
            child: Text('Populares',style: Theme.of(context).textTheme.subhead,)
          ),

          StreamBuilder(
            stream: peliculaProvider.popularesStream,
            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {

                if(snapshot.hasData)  return MovieHorizontal( 
                                                peliculas:snapshot.data, 
                                                siguientePagina: peliculaProvider.getPopulares, 
                                              );

                else return Center(child: CircularProgressIndicator());            
            },
          ),

        ],

      ),

    );

  }
}