import 'package:flutter/material.dart';
import 'package:peliculas/src/models/Peliculas_model.dart';
import 'package:peliculas/src/models/actores_model.dart';
import 'package:peliculas/src/providers/pelicula_provider.dart';

class DetallePage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    final Pelicula pelicula = ModalRoute.of(context).settings.arguments;
    
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppBar( pelicula ),
          SliverList(
            delegate: SliverChildListDelegate([
              SizedBox(height: 10.0,),
              _posterTitulo(context,pelicula),
              _descripcion( pelicula ),
              Container(padding:EdgeInsets.only(left:20.0),child: Text('Cast',style: Theme.of(context).textTheme.subhead)),
              SizedBox(height: 10.0,),
              _crearActores( pelicula )
            ])
          )
        ],
      ),
    );
  }

  Widget _crearAppBar(Pelicula pelicula){

    return SliverAppBar(

      elevation: 2.0,
      expandedHeight: 200.0,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Container(
          padding: EdgeInsets.symmetric(horizontal: 70.0),
          child: Text(
            pelicula.title,
            style: TextStyle(
              color:Colors.white,
              fontSize: 16.0
            ),
            overflow: TextOverflow.ellipsis
          ),
        ),
        background: FadeInImage(
          placeholder: AssetImage('assets/img/loading.gif'), 
          image: NetworkImage(pelicula.getSubportada()),
          fit: BoxFit.cover,
        ),
      ),


    );

  }

  Widget _posterTitulo(BuildContext context, Pelicula pelicula){

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: <Widget>[
          Hero(
            tag:pelicula.uniqueId,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: Image(
                  image: NetworkImage(pelicula.getPortada()),
                  height: 150.0,
                ),
            ),
          ),
          SizedBox(width: 20.0,),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(pelicula.title, style: Theme.of(context).textTheme.title, overflow: TextOverflow.ellipsis,),
                Text(pelicula.originalTitle, style: Theme.of(context).textTheme.subhead,overflow: TextOverflow.ellipsis),
                Row(
                  children: <Widget>[
                    Icon(Icons.star),
                    Text(pelicula.voteAverage.toString(),style: Theme.of(context).textTheme.subhead)
                  ],
                )
              ],
            )
          )

        ],
      )

    );

  }

  Widget _descripcion( Pelicula pelicula ){

    return Container(
      padding: EdgeInsets.symmetric(horizontal:20.0, vertical:10.0),
      child: Text(
        pelicula.overview,
        textAlign: TextAlign.justify,
      ),
    );

  }

  Widget _crearActores( Pelicula pelicula ){

    final peliProvider = new PeliculaProvider();
    
    return FutureBuilder(
      future: peliProvider.getActores(pelicula.id.toString()),
      builder: (BuildContext context, AsyncSnapshot snapshot) {

        if(snapshot.hasData)  return _crearActoresPageView( context, snapshot.data );
        else return Center(child: CircularProgressIndicator());

      },
    );

  }

  Widget _crearActoresPageView(BuildContext context,List<Actor> actores){

    return SizedBox(
      height: 200.0,
      child: PageView.builder(
        pageSnapping: false,
        controller: PageController(
          initialPage: 1,
          viewportFraction: 0.3
        ),
        itemCount: actores.length,
        itemBuilder: (context, i){
          return _actorTarjeta( context,actores[i] );
        },
      )
    );

  }

  Widget _actorTarjeta( BuildContext context,Actor actor ){

    return Container(
      padding: EdgeInsets.only(right: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: FadeInImage(
              placeholder: AssetImage('assets/img/no-image.jpg'), 
              image: NetworkImage(actor.getImgPerfil()),
              height: 150.0,
              fit: BoxFit.fill,
            ),
          ),

          Text(actor.name,style: Theme.of(context).textTheme.subhead,overflow: TextOverflow.ellipsis,)

        ],
      ),
    );

  }
}