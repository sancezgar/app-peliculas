import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:peliculas/src/models/Peliculas_model.dart';
import 'package:peliculas/src/models/actores_model.dart';

class PeliculaProvider{

  String _url = 'api.themoviedb.org';
  String _language = 'es-MX';
  String _apikey = '11f877052e98021a93baebf9b0a89c81';
  int _popularesPage = 0;
  bool _cargando = false;


  //Utilizando streams para actualizar la información de manera en tiempo real

  List<Pelicula> _populares = new List();

  final _popularesStreamController = StreamController<List<Pelicula>>.broadcast();

  Function(List<Pelicula>) get popularesSink => _popularesStreamController.sink.add;
  Stream<List<Pelicula>> get popularesStream => _popularesStreamController.stream;

  //fin Stream

  void disposeStream(){

    _popularesStreamController.close();

  }

  Future<List<Pelicula>> _procesarRespuesta(Uri url) async{

    final resp = await http.get(url);
    final decodeData = json.decode(resp.body);

    final pelicula = new Peliculas.fromJsonList(decodeData['results']);

    return pelicula.lista;

  }

  Future<List<Pelicula>> getEnCines() async{

    final url = Uri.https(_url, '3/movie/now_playing',{
      'language': _language,
      'api_key' : _apikey,
    });

    return await _procesarRespuesta(url);

  }    
  
  Future<List<Pelicula>> getPopulares() async{

    if(_cargando) return[];

    _cargando = true;

    _popularesPage++;

    //print('cargando los siguientes...');

    final url = Uri.https(_url, '3/movie/popular',{
      'language': _language,
      'api_key' : _apikey,
      'page'    : _popularesPage.toString()
    });

    final resp = await _procesarRespuesta(url);

    _populares.addAll(resp);
    popularesSink( _populares );

    _cargando = false;

    return resp;

  }    

  Future<List<Actor>> getActores(String peliculaId)async{
    
    final url = Uri.https(_url, '3/movie/$peliculaId/credits',{
      'language': _language,
      'api_key' : _apikey,
    });

    final resp = await http.get(url);
    final dataDecode = json.decode(resp.body);

    final actores = new Actores.fromJsonList(dataDecode['cast']);

    return actores.lista;
  }

  Future<List<Pelicula>> buscarPelicula(String query) async{

    final url = Uri.https(_url, '3/search/movie',{
      'language': _language,
      'api_key' : _apikey,
      'query'   : query,
    });

    return await _procesarRespuesta(url);

  }  


}