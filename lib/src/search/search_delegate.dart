import 'package:flutter/material.dart';
import 'package:peliculas/src/models/Peliculas_model.dart';
import 'package:peliculas/src/providers/pelicula_provider.dart';


class PeliSearch extends SearchDelegate{

  final peliculaProvider = new PeliculaProvider();

  String seleccion = '';

  final peliculas = [
    'Spiderman',
    'Aquaman',
    'Batman',
    'Shazam',
    'Ironman',
    'Capitan America',
  ];

  final peliculasRecientes = [
    'Spiderman',
    'Capitan America'
  ];

  @override
  List<Widget> buildActions(BuildContext context) {
    // Se coloca las acciones de nuestro AppBar
    return [
      IconButton(
        icon: Icon(Icons.clear), 
        onPressed: (){
          query = '';
        }
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Icono a la Izquierda del AppBar
    return IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow, 
          progress: transitionAnimation
        ),
      onPressed: (){
        close( context, null );
      }
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Se obtiene los Resultados de la busqueda
    return Center(
      child:Container(
        child: Text(seleccion),
      )
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Las sugerencias que aparecen cuando la persona escribe

    if(query.isEmpty){
      return Container();
    }

    return FutureBuilder(
      future: peliculaProvider.buscarPelicula(query),
      builder: (BuildContext context, AsyncSnapshot <List<Pelicula>>snapshot) {

        if( snapshot.hasData ){

          final peliculas = snapshot.data;

          return ListView(
            children: peliculas.map((pelicula){

              return ListTile(
                leading: FadeInImage(
                  placeholder: AssetImage('assets/img/no-image.jpg'), 
                  image: NetworkImage(pelicula.getPortada()),
                  width: 50.0,
                  fit: BoxFit.contain,
                ),
                title: Text(pelicula.title),
                subtitle: Text(pelicula.originalTitle),
                onTap: (){
                  close(context,null);
                  pelicula.uniqueId = '${pelicula.id.toString()} - busqueda';
                  Navigator.pushNamed(context, 'detalle',arguments: pelicula);
                },
              );

            }).toList(),
          );
        }
        else return Center(child: CircularProgressIndicator());

      },
    );
  }

// @override
//   Widget buildSuggestions(BuildContext context) {
//     // Las sugerencias que aparecen cuando la persona escribe

//     final listaSugerida = ( query.isEmpty ) ? peliculasRecientes
//                                             : peliculas.where(
//                                               (p) => p.toLowerCase().startsWith(query.toLowerCase())
//                                               ).toList();


//     return ListView.builder(
//       itemCount: listaSugerida.length,
//       itemBuilder: (context, i){

//         return ListTile(
//           leading: Icon(Icons.movie),
//           title: Text(listaSugerida[i]),
//           onTap: (){
//             seleccion = listaSugerida[i];
//             showResults(context);

//           },
//         );

//       },
//     );
//   }


}