class Actores{

  List<Actor> lista = new List();

  Actores();

  Actores.fromJsonList(List<dynamic> jsonList){

    if(jsonList==null)return;

    for(var item in jsonList){

      final actor = Actor.fromJsonMap(item);

      lista.add(actor);

    }

  }

}

class Actor {
  int castId;
  String character;
  String creditId;
  int gender;
  int id;
  String name;
  int order;
  String profilePath;

  Actor({
    this.castId,
    this.character,
    this.creditId,
    this.gender,
    this.id,
    this.name,
    this.order,
    this.profilePath,
  });

  Actor.fromJsonMap(Map<String,dynamic>json){

    castId      = json['cast_id'];
    character   = json['character'];
    creditId    = json['credit_id'];
    gender      = json['gender'];
    id          = json['id'];
    name        = json['name'];
    order       = json['order'];
    profilePath = json['profile_path'];

  }

  String getImgPerfil(){

    if(profilePath==null) return 'https://image.shutterstock.com/image-vector/no-image-available-icon-photo-260nw-1251146734.jpg';
    else return 'https://image.tmdb.org/t/p/w500/$profilePath';

  }
}

